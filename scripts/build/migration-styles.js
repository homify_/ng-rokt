const fs = require('fs-extra');
const path = require('path');

const sourcePath = path.resolve(__dirname, `../../node_modules/ng-zorro-antd`);
const targetPath = path.resolve(__dirname, `../../publish/style`);
const publishPath = path.resolve(__dirname, '../../publish');
const componentsPath = path.resolve(__dirname, '../../components');

fs.mkdirsSync(targetPath);

fs.copySync(path.resolve(componentsPath, 'style'), targetPath);
fs.copySync(path.resolve(sourcePath, `ng-zorro-antd.css`), path.resolve(publishPath, `ng-rokt.css`));
fs.copySync(path.resolve(sourcePath, `ng-zorro-antd.min.css`), path.resolve(publishPath, `ng-rokt.min.css`));
