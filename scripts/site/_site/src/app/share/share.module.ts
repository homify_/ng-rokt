import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  NgRoktModule
} from 'ng-rokt';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ColorSketchModule } from 'ngx-color/sketch';

import { NzCodeBoxModule } from './nz-codebox/nz-codebox.module';
import { NzHighlightModule } from './nz-highlight/nz-highlight.module';
import { NzNavBottomModule } from './nz-nav-bottom/nz-nav-bottom.module';
import { NgZorroAntdModule } from 'ng-zorro-antd';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    NgRoktModule,
    NzCodeBoxModule,
    NzHighlightModule,
    NzNavBottomModule,
    // third libs
    ColorSketchModule,
    NgZorroAntdModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgRoktModule,
    NzCodeBoxModule,
    NzHighlightModule,
    NzNavBottomModule,
    // third libs
    InfiniteScrollModule,
    ColorSketchModule,
    NgZorroAntdModule
  ]
})
export class ShareModule { }
