const path = require('path');
const fs = require('fs');
const capitalizeFirstLetter = require('./capitalize-first-letter');
const camelCase = require('./camelcase');
const replacePrefix = require('./replace-prefix');
const PrismAngular = require('./angular-language-marked');

module.exports = function (showCaseComponentPath, result) {
  if (result.pageDemo) {
    fs.writeFileSync(path.join(showCaseComponentPath, `page.component.ts`), result.pageDemo.enCode);
  }
  const demoTemplate = generateTemplate(result);
  fs.writeFileSync(path.join(showCaseComponentPath, `index.html`), demoTemplate.en);
  const demoComponent = generateDemoComponent(result);
  fs.writeFileSync(path.join(showCaseComponentPath, `component.ts`), demoComponent.en);
  const demoModule = generateDemoModule(result);
  fs.writeFileSync(path.join(showCaseComponentPath, `index.module.ts`), demoModule);
};

function generateDemoModule(content) {
  const demoModuleTemplate = String(fs.readFileSync(path.resolve(__dirname, '../template/demo-module.template.ts')));
  const component = content.name;
  const demoMap = content.demoMap;
  let imports = '';
  let declarations = '';
  let entryComponents = [];
  for (const key in demoMap) {
    const declareComponents = [`NzDemo${componentName(component)}${componentName(key)}Component`];
    const entries = retrieveEntryComponents(demoMap[key] && demoMap[key].ts);
    entryComponents.push(...entries);
    declareComponents.push(...entries);
    imports += `import { ${declareComponents.join(', ')} } from './${key}';\n`;
    declarations += `\t\t${declareComponents.join(',\n\t')},\n`;
  }
  imports += `import { NzDemo${componentName(component)}Component } from './component';\n`;
  declarations += `\t\tNzDemo${componentName(component)}Component,\n`;
  if (content.pageDemo) {
    imports += `import { NzPageDemo${componentName(component)}Component } from './page.component';\n`;
    declarations += `\t\tNzPageDemo${componentName(component)}Component,\n`;
  }
  return demoModuleTemplate.replace(/{{imports}}/g, imports).replace(/{{declarations}}/g, declarations).replace(/{{component}}/g, componentName(component)).replace(/{{entryComponents}}/g, entryComponents.join(',\n'));
}

function componentName(component) {
  return camelCase(capitalizeFirstLetter(component));
}

function generateComponentName(component) {
  return `NzDemo${componentName(component)}Component`
}

function generateDemoComponent(content) {
  const demoComponentTemplate = String(fs.readFileSync(path.resolve(__dirname, '../template/demo-component.template.ts')));
  const component = content.name;
  const demoMap = content.demoMap;
  let code = '';
  let rawCode = '';
  for (const key in demoMap) {
    let prismCode = PrismAngular.highlight(demoMap[key].ts, Prism.languages['angular']);
    prismCode = replacePrefix(prismCode);
    const angularCode = encodeURIComponent(prismCode);
    code += `\t${camelCase(key)} = \`${angularCode}\`\n`;
    rawCode += `\t${camelCase(key)}Raw = require('!!raw-loader!./${key}.ts');\n`;
  }
  let output = demoComponentTemplate;
  output = output.replace(/{{component}}/g, component);
  output = output.replace(/{{code}}/g, code);
  output = output.replace(/{{rawCode}}/g, rawCode);
  let enOutput = output;
  enOutput = enOutput.replace(/{{componentName}}/g, generateComponentName(component, 'en'));
  enOutput = enOutput.replace(/{{language}}/g, 'en');
  return {
    en: enOutput,
  };
}

function generateTemplate(result) {
  const generateTitle = require('./generate.title');
  const innerMap = generateExample(result);
  const titleMap = {
    en: generateTitle(result.docEn.meta.title, '', result.docEn.path)
  };
  const name = result.name;
  const hasPageDemo = !!result.pageDemo;
  return {
    en: wrapperAll(generateToc('en-US', result.name, result.demoMap), wrapperHeader(titleMap.en, result.docEn.whenToUse, 'en', innerMap.en, hasPageDemo, name) + wrapperAPI(result.docEn.api))
  }
}

function wrapperAPI(content) {
  return `<section class="markdown api-container" ngNonBindable>${content}</section>`
}

function wrapperHeader(title, whenToUse, language, example, hasPageDemo, name) {
  if (example) {
    return `<section class="markdown">
	${title}
	<section class="markdown" ngNonBindable>
		${whenToUse}
	</section>
	${hasPageDemo ? `<section class="page-demo"><nz-page-demo-${name}></nz-page-demo-${name}></section>` : ''}
	<h2>
		<span>${language === 'zh' ? '代码演示' : 'Examples'}</span>
		<i nz-icon type="appstore" class="code-box-expand-trigger" title="${language === 'zh' ? '展开全部代码' : 'expand all code'}" (click)="expandAllCode()"></i>
	</h2>
</section>${example}`
  } else {
    return `<section class="markdown">
	${title}
	<section class="markdown">
		${whenToUse}
	</section></section>`
  }

}

function wrapperAll(toc, content) {
  return `<article>${toc}${content}</article>`
}

function generateToc(language, name, demoMap) {
  let linkArray = [];
  for (const key in demoMap) {
    linkArray.push(
      {
        content: `<nz-link nzHref="#components-${name}-demo-${key}" nzTitle="${demoMap[key].meta.title[language]}"></nz-link>`,
        order  : demoMap[key].meta.order
      }
    );
  }
  linkArray.sort((pre, next) => pre.order - next.order);
  const links = linkArray.map(link => link.content).join('');
  return `<nz-affix class="toc-affix" [nzOffsetTop]="16">
    <nz-anchor [nzAffix]="false" nzShowInkInFixed (nzClick)="goLink($event)">
      ${links}
    </nz-anchor>
  </nz-affix>`;
}

function generateExample(result) {
  const demoMap = result.demoMap;
  const isEnUnion = result.docEn.meta.cols;
  const templateSplit = String(fs.readFileSync(path.resolve(__dirname, '../template/example-split.template.html')));
  const templateUnion = String(fs.readFileSync(path.resolve(__dirname, '../template/example-union.template.html')));
  let demoList = [];
  for (const key in demoMap) {
    demoList.push(Object.assign(
      {name: key}, demoMap[key]
    ))
  }
  demoList = demoList.sort((pre, next) => pre.meta.order - next.meta.order);
  let firstEnPart = '';
  let secondEnPart = '';
  let enPart = '';
  demoList.forEach((item, index) => {
    enPart += item.enCode;
    if (index % 2 === 0) {
      firstEnPart += item.enCode;
    } else {
      secondEnPart += item.enCode;
    }
  });
  return {
    en: (isEnUnion ? templateUnion.replace(/{{content}}/g, enPart) : templateSplit.replace(/{{first}}/g, firstEnPart).replace(/{{second}}/g, secondEnPart))
  }
}

function retrieveEntryComponents(plainCode) {
  var matches = (plainCode + '').match(/^\/\*\s*?entryComponents:\s*([^\n]+?)\*\//) || [];
  if (matches[1]) {
    return matches[1].split(',').map(className => className.trim()).filter((value, index, self) => value && self.indexOf(value) === index);
  }
  return [];
}
