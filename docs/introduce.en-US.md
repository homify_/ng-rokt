---
order: 0
title: Rokt Living Style Guides
---

Documentation for the Rokt Angular Component Library

<div class="pic-plus">
  <img width="150" src="https://image4.owler.com/logo/rokt_owler_20171219_171644_original.png">
  <span>+</span>
  <img height="150" src="https://img.alicdn.com/tfs/TB1Z0PywTtYBeNjy1XdXXXXyVXa-186-200.svg">
</div>

<style>
.pic-plus > * {
  display: inline-block !important;
  vertical-align: middle;
}
.pic-plus span {
  font-size: 30px;
  color: #aaa;
  margin: 0 20px;
}
</style>

## Features

- An enterprise-class UI design language for web applications.
- A set of high-quality Angular components out of the box.
- Written in TypeScript with complete defined types.

## Environment Support

Modern browsers and Internet Explorer 11+ (with [polyfills](https://angular.io/guide/browser-support))

## Angular Support

Now Supports Angular `^6.0.0`.

## Installation

```bash
$ npm install ng-rokt
```