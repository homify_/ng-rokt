---
order: 1
title: Getting Started
---

## First Local Development

### 1. Installation

```bash
$ npm install ng-rokt
```

### 2. Import module

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgRoktModule, NZ_I18N, en_US } from 'ng-rokt';
import { AppComponent } from './app.component';

/** config angular i18n **/
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    /** import ng-rokt root module，you should import NgRoktModule sub module **/
    NgRoktModule
  ],
  bootstrap: [ AppComponent ],
  /** config ng-rokt i18n **/
  providers   : [ { provide: NZ_I18N, useValue: en_US } ]
})
export class AppModule { }
```

### 5. Add Styles and SVG Assets

And import style and SVG icon assets file link in `angular.json`.

You can get more info about how to customize styles at [customize theme](/docs/customize-theme/en) part.

```json
{
  "assets": [
    ...
    {
      "glob": "**/*",
      "input": "./node_modules/@ant-design/icons-angular/src/inline-svg/",
      "output": "/assets/"
    }
  ],
  "styles": [
    ...
    "node_modules/ng-rokt/ng-rokt.min.css"
  ]
}
```
