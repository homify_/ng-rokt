---
category: Components
type: OP
title: Advanced Options
---

Advanced Options is used in Campaign/Audience Creation Wizard.

## API

### nr-advanced-options

| Property | Description | Type | Default |
| -------- | ----------- | ---- | ------- |
| `[duration]` | Duration of animation | number | 600 |
| `[showArrow]` | Display arrow or not | boolean | true |
