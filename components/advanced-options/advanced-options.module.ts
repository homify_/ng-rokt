import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdvancedOptionsComponent } from './advanced-options.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    AdvancedOptionsComponent
  ],
  exports: [
    AdvancedOptionsComponent
  ]
})
export class AdvancedOptionsModule { }
