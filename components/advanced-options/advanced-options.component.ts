import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterContentChecked, Component, ElementRef, Input, ViewChild } from '@angular/core';

const timing = animate('{{duration}}ms cubic-bezier(0.25, 0.8, 0.25, 1)');

@Component({
  selector: 'nr-advanced-options',
  templateUrl: './advanced-options.component.html',
  styleUrls: ['./advanced-options.component.scss'],
  animations: [
    trigger('boxDisplay', [
      state('expanded', style({height: '{{height}}px'}), {params: {height: 0}}),
      state('collapsed', style({height: 0})),
      transition('* => *', timing)
    ]),
    trigger('gearDisplay', [
      state('expanded', style({transform: 'rotate(-180deg)'})),
      state('collapsed', style({transform: 'rotate(0)'})),
      transition('* => *', timing)
    ]),
    trigger('arrowDisplay', [
      state('expanded', style({transform: 'rotateX(-180deg)'})),
      state('collapsed', style({transform: 'rotateX(0)'})),
      transition('* => *', timing)
    ])
  ]
})
export class AdvancedOptionsComponent implements AfterContentChecked {

  @ViewChild('contentBox') contentBox: ElementRef;
  @Input() duration: number = 600;
  @Input() showArrow: boolean = true;

  contentState: 'collapsed' | 'expanded' = 'collapsed';
  contentHeight = 0;

  ngAfterContentChecked() {
    this.contentHeight = this.contentBox.nativeElement.offsetHeight;
  }
}
