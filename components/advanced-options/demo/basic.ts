import { Component } from '@angular/core';

@Component({
  selector: 'nz-demo-advanced-options-basic',
  template: `
    <div class="notes">
      <nr-advanced-options [duration]="delay" [showArrow]="showArrow">
        <label>Show Arrow: <input type="checkbox" [(ngModel)]="showArrow"></label><br>
        <label>Delay time: <input type="number" min="100" max="5000" step="100" [(ngModel)]="delay"></label>
      </nr-advanced-options>
    </div>
  `
})
export class NzDemoAdvancedOptionsBasicComponent {
  showArrow = true;
  delay = 500;
}
