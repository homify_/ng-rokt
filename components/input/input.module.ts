import { NgModule } from '@angular/core';
import { NzInputModule } from 'ng-zorro-antd';

@NgModule({
  imports: [
    NzInputModule
  ],
  exports: [
    NzInputModule
  ]
})
export class NrInputModule { }
