import {
   Directive
} from '@angular/core';
import { NzCheckboxComponent } from 'ng-zorro-antd';

@Directive({
  selector : '[nr-checkbox]'
})
export class CheckboxComponent extends NzCheckboxComponent {
}
