import { NgModule } from '@angular/core';
import { CheckboxComponent } from './checkbox.component';
import { NzCheckboxModule } from 'ng-zorro-antd';

@NgModule({
  imports     : [ NzCheckboxModule ],
  declarations: [
    CheckboxComponent
  ],
  exports     : [
    CheckboxComponent
  ]
})
export class CheckboxModule {
}
