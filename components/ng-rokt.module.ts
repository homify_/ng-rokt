import { ModuleWithProviders, NgModule } from '@angular/core';

import { ChartsModule } from '@progress/kendo-angular-charts';
import { NzCheckboxModule,
  NzIconModule,
  NzInputNumberModule, NzWaveModule
  } from 'ng-zorro-antd';
import { AdEditorModule } from './ad-editor';
import { AdvancedOptionsModule } from './advanced-options';
import { NrInputModule } from './input';

export * from './input';

@NgModule({
  exports: [
    NrInputModule,
    NzInputNumberModule,
    NzIconModule,
    NzWaveModule,
    AdEditorModule,
    AdvancedOptionsModule,
    NzCheckboxModule,
    ChartsModule
  ]
})
export class NgRoktModule {
  /**
   * @deprecated Use `NgRoktModule` instead.
   */
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgRoktModule
    };
  }
}
