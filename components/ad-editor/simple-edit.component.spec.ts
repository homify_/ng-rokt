import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { By } from '@angular/platform-browser';

import { SimpleEditComponent } from './simple-edit.component';

describe('SimpleEditComponent', () => {
  let component: SimpleEditComponent;
  let fixture: ComponentFixture<SimpleEditComponent>;
  let de, editor: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule],
      declarations: [SimpleEditComponent],
      providers: []
    })
    .compileComponents();

    fixture = TestBed.createComponent(SimpleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    de = fixture.debugElement;
    editor = de.query(By.css('div.editor'));
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should remove styles/formatting when pasting data', async(() => {
    const dt = new DataTransfer();
    dt.setData('text/html', '<b>Hello World!</b>');
    dt.setData('text/plain', 'Hello World!');

    const evt = new ClipboardEvent('paste');
    spyOnProperty(evt, 'clipboardData', 'get').and.returnValue(dt);
    spyOn(component, 'paste').and.callThrough();

    editor.nativeElement.dispatchEvent(evt);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.paste).toHaveBeenCalled();
      expect(editor.nativeElement.innerHTML).toBe('Hello World!');
    });
  }));
});
