export interface EditData {
  html: string;
  readonly text: string;
  readonly count: number;
}
