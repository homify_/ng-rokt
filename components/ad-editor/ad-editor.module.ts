import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdEditorComponent } from './ad-editor.component';
import { AdEditorToolbarComponent } from './ad-editor-toolbar.component';
import { SimpleEditComponent } from './simple-edit.component';
import { TextContentLengthValidator } from './text-content-length.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    AdEditorComponent,
    AdEditorToolbarComponent,
    SimpleEditComponent,
    TextContentLengthValidator,
  ],
  exports: [
    AdEditorComponent,
    AdEditorToolbarComponent,
    SimpleEditComponent,
    TextContentLengthValidator,
  ],
})
export class AdEditorModule { }
