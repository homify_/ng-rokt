import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Component, SimpleChange, DebugElement, forwardRef } from '@angular/core';
import { TextContentLengthValidator } from './text-content-length.directive';
import { FormsModule, NG_VALIDATORS } from '@angular/forms';
import { CommonModule } from '@angular/common';

const failureHandler = error => fail('expecting success, got an error');

@Component({
  template: `
  <input name="f1" [maxContentLength]="maxContentLength1" #f1Model="ngModel"
         [(ngModel)]="output1" opTextContentLength>
  <input name="f2" [maxContentLength]="maxContentLength2" #f2Model="ngModel"
         [(ngModel)]="output2" opTextContentLength>
`
})
export class ParentComponent {
  maxContentLength1 = 0;
  maxContentLength2 = 10;
  output1 = 'ABCDEFGHIJKLMNOP';
  output2 = 'abcdefghijklmn';
}

describe('TextContentLengthValidator', () => {
  const rendererSpy = jasmine.createSpyObj('Renderer', ['createElement']);
  const directive = new TextContentLengthValidator(rendererSpy);
  const fakeChange1 = {maxContentLength: new SimpleChange(0, 12, true)};
  const fakeChange2 = {maxContentLength: new SimpleChange(12, 6, false)};

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should return validator functions', () => {
    const fn = directive.createTextContentLengthValidator(15);
    expect(typeof fn).toBe('function');
  });

  it('should update validator function when mexLength changes', () => {
    directive.maxContentLength = 12;
    directive.ngOnChanges(fakeChange1);
    const fn1 = directive.validateFn;
    directive.ngOnChanges(fakeChange2);
    const fn2 = directive.validateFn;
    expect(fn2).not.toEqual(fn1);
  });

  describe('When used in component', () => {
    let component: ParentComponent;
    let fixture: ComponentFixture<ParentComponent>;
    let de: DebugElement;
    let f1Model, f2Model;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
          CommonModule,
          FormsModule,
        ],
        declarations: [
          ParentComponent,
          TextContentLengthValidator,
        ],
        providers: [
          {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => TextContentLengthValidator),
            multi: true,
          }
        ],
      }).compileComponents();
    }));

    beforeEach(async(() => {
      fixture = TestBed.createComponent(ParentComponent);
      component = fixture.componentInstance;
      de = fixture.debugElement;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        f1Model = de.query(By.css('input[name=f1]')).references['f1Model'];
        f2Model = de.query(By.css('input[name=f2]')).references['f2Model'];
      }, failureHandler);
    }));

    it('should accept any input if maxContentLength is set to 0', async(() => {
      component.output1 = 'today is a busy day';
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(f1Model.valid).toBe(true);
      });
    }));

    it('should reject longer input if maxContentLength is non-zero', async(() => {
      component.output2 = 'anyone knows how to do it';
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(f2Model.valid).toBe(false);
      });
    }));

    it('should accept shorter input if maxContentLength is non-zero', async(() => {
      component.output2 = 'anyone';
      component.maxContentLength2 = 8;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(f2Model.valid).toBe(true);
      });
    }));

    it('should validate textContent, not html', async(() => {
      component.output2 = '<b>abcde</b><!-- this is longer -->';
      component.maxContentLength2 = 5;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(f2Model.valid).toBe(true);
      });
    }));

    describe('When input is the same, and maxContentLength changes', () => {
      beforeEach(async(() => {
        component.output2 = 'anyone';
      }));

      it('should vary validation result based on given maxContentLength', async(() => {
        component.maxContentLength2 = 5;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(f2Model.valid).toBe(false);
        });
      }));

      it('should generate new validator using new value', async(() => {
        component.maxContentLength2 = 14;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(f2Model.valid).toBe(true);
        });
      }));
    });
  });
});
