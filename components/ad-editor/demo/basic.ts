import { Component } from '@angular/core';

@Component({
  selector: 'nz-demo-ad-editor-basic',
  template: `
    <nr-ad-editor [advancedModeEnabled]="advancedModeEnabled"
                  opTextContentLength
                  [maxContentLength]="maxContentLength"
                  [(ngModel)]="adText"
                  [placeholder]="placeholder"></nr-ad-editor>
  `
})
export class NzDemoAdEditorBasicComponent {
  advancedModeEnabled = true;
  maxContentLength = 150;
  adText = '';
  placeholder = 'Please enter ad text here ...';
}
