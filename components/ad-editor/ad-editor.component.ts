import { forwardRef, Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { EditData } from './edit-data';

@Component({
  selector: 'nr-ad-editor',
  templateUrl: './ad-editor.component.html',
  styleUrls: ['./ad-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AdEditorComponent),
      multi: true
    }
  ]
})
export class AdEditorComponent implements ControlValueAccessor {

  @HostBinding('class.advanced')
  advancedMode = false;

  @HostBinding('class.advanced-enabled')
  _advancedModeEnabled = false;

  @Input()
  get advancedModeEnabled() {
    return this._advancedModeEnabled;
  }

  set advancedModeEnabled(enabled) {
    this._advancedModeEnabled = enabled;
    if (!enabled && this.advancedMode) {
      this.advancedMode = false;
    }
  }

  @Input()
  maxContentLength ? = 140;

  @Input()
  placeholder ? = '';

  @Output()
  dataChange = new EventEmitter<EditData>();

  viewValue = '';

  get htmlOutput() {
    return this.viewValue;
  }

  set htmlOutput(newValue) {
    if (newValue !== null) {
      this.writeValue(newValue);
    }
  }

  _onChange = (_: any) => {};
  _onTouched = (_: any) => {};

  writeValue(modelValue: any): void {
    this.viewValue = modelValue;
    this._onChange(modelValue);
  }

  registerOnChange(fn: (_: any) => void): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: (_: any) => void): void {
    this._onTouched = fn;
  }
}
