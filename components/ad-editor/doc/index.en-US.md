---
category: Components
type: OP
title: Ad Editor
---

Ad editor will be used in Campaign/Audience Creation Wizard.

## API

### nr-ad-editor

| Property | Description | Type | Default |
| -------- | ----------- | ---- | ------- |
| `[advancedModeEnabled]` | advancedModeEnabled | boolean | false |
| `[maxContentLength]` | max content length | number | 140 |
| `[placeholder]` | placeholder | string | - |
| `(dataChange)` | emit if data change | `EventEmitter<EditData>` | - |