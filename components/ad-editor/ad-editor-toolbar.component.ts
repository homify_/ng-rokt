import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'op-ad-editor-toolbar',
  templateUrl: './ad-editor-toolbar.component.html',
  styleUrls: ['./ad-editor-toolbar.component.scss']
})
export class AdEditorToolbarComponent {

  @Output()
  reset = new EventEmitter<void>();

  constructor() { }

  clear() {
    this.reset.emit();
  }
}
