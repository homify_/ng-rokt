import {
  Component, HostBinding, Input, Output, EventEmitter, ViewChild, AfterViewInit,
  ElementRef, forwardRef
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EditData } from './edit-data';

@Component({
  selector: 'op-simple-edit',
  templateUrl: './simple-edit.component.html',
  styleUrls: ['./simple-edit.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SimpleEditComponent),
      multi: true
    }
  ]
})
export class SimpleEditComponent implements AfterViewInit, ControlValueAccessor {

  @HostBinding('class.readonly')
  @Input()
  readonly ? = false;

  @Input()
  maxContentLength ? = 0;

  @Input()
  placeholder ? = '';

  @Output()
  dataChange = new EventEmitter<EditData>();

  characterCount$: Observable<number>;

  @ViewChild('editor')
  protected editorRef: ElementRef;

  protected editor: HTMLElement;

  protected set html(val) {
    if (this.editor) {
      if (this.editor.innerHTML !== val) {
        this.editor.innerHTML = val;
      }
      const html = this.editor.innerHTML;
      const text = this.editor.textContent;
      const count = text.length;
      this.dataChange.emit({html, text, count});
    }
    this.propagateChange(val);
  }

  propagateChange = (_: any) => {};

  constructor() {
    this.characterCount$ = this.dataChange.asObservable().pipe(
      map(editData => editData.count)
    );
  }

  ngAfterViewInit() {
    this.editor = this.editorRef.nativeElement;
  }

  writeValue(value: any) {
    this.html = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void { }

  update() {
    this.html = this.editor.innerHTML;
  }

  paste(e: ClipboardEvent) {
    e.preventDefault();
    this.html = e.clipboardData.getData('text/plain');
  }

  clear() {
    this.html = '';
  }
}
