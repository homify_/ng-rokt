import { Directive, Renderer2, Input, OnChanges, forwardRef } from '@angular/core';
import { Validator, FormControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[opTextContentLength]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => TextContentLengthValidator),
      multi: true
    }
  ]
})
export class TextContentLengthValidator implements OnChanges, Validator {

  @Input()
  maxContentLength ? = 0;

  validateFn: Function;

  constructor(protected renderer: Renderer2) {}

  createTextContentLengthValidator(maxContentLength) {
    const maxLength = +maxContentLength;
    return function(c: FormControl) {
      const div = this.renderer.createElement('div');
      div.innerHTML = (c.value || '');
      const textContent = div.textContent;
      const count = textContent.length;
      const err = {
        textContentLengthError: {
          given: c.value,
          count,
          max: maxLength,
        }
      };
      return (maxLength > 0 && count > maxLength) ? err : null;
    };
  }

  ngOnChanges(changes) {
    if (changes.maxContentLength) {
      this.validateFn = this.createTextContentLengthValidator(this.maxContentLength || 0);
    }
  }

  validate(c: FormControl) {
    return this.validateFn(c);
  }
}
