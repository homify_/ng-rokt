---
category: Components
type: Charts
title: Chart
---

The Chart renders a wide range of high-quality data visualizations.

## API

### nr-chart

More docs see [Kendo](https://www.telerik.com/kendo-angular-ui/components/charts/api/ChartComponent/#toc-axisdefaults).

| Property | Description | Type | Default |
| -------- | ----------- | ---- | ------- |
| `[axisDefaults]` | The default options for all Chart axes. Accepts the options which are supported by categoryAxis, valueAxis, xAxis, and yAxis. | `AxisDefaults` | - |
| `[categoryAxis]` | The configuration options of the category axis. | `CategoryAxis | CategoryAxis[]` | - |
| `[chartArea]` | The configuration options of the Chart area. Represents the entire visible area of the Chart.| `ChartArea` | - |
| `[legend]` | The configuration options of the Chart legend. | `legend` | - |
| `[panes]` | The configuration of the Chart panes. Panes are used to split the Chart into two or more parts. The panes are ordered from top to bottom. To associate each axis with a pane, set its pane option to the name of the desired pane. Axis that do not have a specified pane are placed in the top (default) pane. To move a series to the desired pane, associate them with an axis. | `Pane[]` | - |
| `[pannable]` | Specifies if the Chart can be panned. | `boolean | DragAction` | - |
| `[plotArea]` | TThe configuration options of the plot area. The plot area is the area which displays the series. | `PlotArea` | - |
| `[renderAs]` | Sets the preferred rendering engine. If not supported by the browser, the Chart switches to the first available mode. The supported values are: "svg"—If available, renders the component as an inline .svg file. "canvas"—If available, renders the component as a canvas element.`| `ChartArea` | - |
| `[resizeRateLimit]` | Limits the automatic resizing of the Chart. Sets the maximum number of times per second that the component redraws its content when the size of its container changes. | number | 10 |
| `[series]` | The configuration of the Chart series. The series type is determined by the value of the type field. If a type value is missing, the Chart renders the type that is specified in seriesDefaults. Some options accept functions as arguments. These arguments are evaluated for each point which is supplied as a parameter. If no value is returned, the Chart uses the theme or seriesDefaults values. | `Series[]` | - |
| `[seriesColors]` | The configuration options of the Chart area. Represents the entire visible area of the Chart.| `string[]` | - |

